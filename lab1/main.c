#include <stdlib.h>
#include "database/mem.h"
#include "database/database_manager.h"
#include "test.h"


int main(int argc, char *argv[]) {
    char *db_name = "database.qdb";
    RC rc = RC_OK;

    Connection connection = {0};
    rc = database_open(db_name, &connection);
    throw_if_not_ok(rc);

   test(&connection);

    rc = database_close(&connection);
    return rc;
}
