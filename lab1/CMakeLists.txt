cmake_minimum_required(VERSION 3.20)
project(low_level_programming_task1 C)

set(CMAKE_C_STANDARD 11)
set(EXECUTABLE low_level_programming_task1)

add_executable(${EXECUTABLE}
        main.c
        database/mem.h
        database/model/block_model.c
        database/model/block_model.h
        database/model/string_model.c
        database/model/string_model.h
        database/connection.h
        database/connection.c
        database/buffer_manager.h
        database/buffer_manager.c
        database/database_manager.h
        database/database_manager.c
        database/serialize/string_serialize.h
        database/serialize/string_serialize.c
        database/serialize/block_serialize.h
        database/serialize/block_serialize.c
        database/serialize/util.h
        database/visualize.h
        database/visualize.c
        database/model/schema_model.c
        database/model/schema_model.h
        database/model/attribute_model.c
        database/model/attribute_model.h
        database/model/data_type_model.h
        database/serialize/schema_serialize.c
        database/serialize/schema_serialize.h
        database/serialize/attribute_serialize.c
        database/serialize/attribute_serialize.h
        database/model/database_model.h
        database/model/database_model.c
        database/data_structure/block_heap.c
        database/data_structure/block_heap.h
        database/data_structure/block_node.c
        database/data_structure/block_node.h
        database/model/node_model.c
        database/model/node_model.h
        database/model/value_model.c
        database/model/value_model.h
        database/serialize/value_serialize.c
        database/serialize/value_serialize.h
        database/serialize/node_serialize.c
        database/serialize/node_serialize.h
        database/model/models.h
        database/serialize/serializers.h
        database/condition.h
        database/comparation.h
        database/serialize/util.c
        database/data_manipulation/crud_util.h
        database/data_manipulation/attribute_crud.c
        database/data_manipulation/attribute_crud.h
        database/data_manipulation/node_crud.c
        database/data_manipulation/node_crud.h
        database/data_manipulation/schema_crud.c
        database/data_manipulation/schema_crud.h
        database/data_manipulation/value_crud.c
        database/data_manipulation/value_crud.h
        database/data_manipulation/string_crud.c
        database/data_manipulation/string_crud.h
        database/data_manipulation/cruds.h database/model/link_model.c database/model/link_model.h database/serialize/link_serialize.c database/serialize/link_serialize.h database/data_manipulation/link_crud.c database/data_manipulation/link_crud.h database/query_manager.c database/query_manager.h database/model/result_model.h database/model/result_model.c database/data_structure/vector.h database/data_structure/generator.c database/data_structure/generator.h database/data_structure/vector_abstract.h database/data_structure/vector.c test.h)

target_link_libraries(${EXECUTABLE} m)